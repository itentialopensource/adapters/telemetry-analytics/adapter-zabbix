
## 0.3.0 [08-22-2022]

* minor/ADAPT-2250

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!5

---

## 0.2.0 [06-01-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!4

---

## 0.1.4 [10-05-2021]

- Trying url change to see if it fixes pipeline issue

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!3

---

## 0.1.3 [10-05-2021] & 0.1.2 [10-05-2021]

- Removed the adapter auto setting auth as that causes issues

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!2

---

## 0.1.1 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!1

---
