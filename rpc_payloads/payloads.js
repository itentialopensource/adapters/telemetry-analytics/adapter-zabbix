const jsonrpcVersion = '2.0';

const payloads = {
  getVersion: {
    jsonrpc: jsonrpcVersion,
    method: 'apiinfo.version',
    params: {}
  },

  authenticate: {
    jsonrpc: jsonrpcVersion,
    method: 'user.login',
    params: {
    }
  },

  getTriggers: {
    jsonrpc: jsonrpcVersion,
    method: 'trigger.get',
    params: {
      sortorder: 'DESC',
      sortfield: 'priority',
      filter: {
        value: 1
      },
      output: [
        'triggerid',
        'description',
        'lastchange'
      ]
    }
  },

  getItem: {
    jsonrpc: jsonrpcVersion,
    method: 'item.get',
    params: {
      output: 'extend'
    }
  },

  getHistory: {
    jsonrpc: jsonrpcVersion,
    method: 'history.get',
    params: {
      output: 'extend',
      sortorder: 'DESC',
      sortfield: 'clock',
      history: 3
    }
  },

  getMap: {
    jsonrpc: jsonrpcVersion,
    method: 'map.get',
    params: {
      selectLinks: 'extend',
      selectSelements: 'extend',
      output: 'extend'
    }
  }
};

module.exports = payloads;
