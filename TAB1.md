# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Zabbix System. The API that was used to build the adapter for Zabbix is usually available in the report directory of this adapter. The adapter utilizes the Zabbix API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Zabbix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Zabbix. With this adapter you have the ability to perform operations such as:

- Add Device to Monitoring
- Automated network data gathering for diagnostics,
- Verify network connectivity

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
