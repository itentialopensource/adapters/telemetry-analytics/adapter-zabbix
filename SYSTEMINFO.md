# Zabbix

Vendor: Zabbix
Homepage: https://www.zabbix.com/

Product: Zabbix
Product Page: https://www.zabbix.com/

## Introduction
We classify Zabbix into the Service Assurance domain as Zabbix is a monitoring software for networks, servers, applications, and cloud services that provides real-time monitoring, alerting, and visualization capabilities.

## Why Integrate
The Zabbix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Zabbix. With this adapter you have the ability to perform operations such as:

- Add Device to Monitoring
- Automated network data gathering for diagnostics,
- Verify network connectivity

## Additional Product Documentation
The [API documents for Zabbix](https://www.zabbix.com/documentation/current/en/manual/api)
