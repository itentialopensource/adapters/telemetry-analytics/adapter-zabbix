
## 0.5.5 [10-15-2024]

* Changes made at 2024.10.14_20:51PM

See merge request itentialopensource/adapters/adapter-zabbix!23

---

## 0.5.4 [09-20-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-zabbix!21

---

## 0.5.3 [08-14-2024]

* Changes made at 2024.08.14_19:08PM

See merge request itentialopensource/adapters/adapter-zabbix!20

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_15:10PM

See merge request itentialopensource/adapters/adapter-zabbix!19

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:23PM

See merge request itentialopensource/adapters/adapter-zabbix!18

---

## 0.5.0 [07-03-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!17

---

## 0.4.3 [03-27-2024]

* Changes made at 2024.03.27_13:19PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!16

---

## 0.4.2 [03-12-2024]

* Changes made at 2024.03.12_11:01AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!15

---

## 0.4.1 [02-27-2024]

* Changes made at 2024.02.27_11:37AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!14

---

## 0.4.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-zabbix!13

---
